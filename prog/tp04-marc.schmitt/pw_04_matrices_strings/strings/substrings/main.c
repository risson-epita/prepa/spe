#include <stdio.h>
#include <err.h>

size_t strlen(char *s)
{
    size_t len;
    for (len = 0; s[len] != '\0'; ++len)
        /* nothing */;
    return len;
}

int streq(char str1[], char str2[], size_t l1, size_t l2)
{
    if (l1 != l2)
        return 0;
    for (size_t i = 0; i < l1; ++i)
    {
        if (str1[i] != str2[i])
            return 0;
    }
    return 1;
}

int strstrn(char str[], char substr[])
{
    size_t slen, sublen;
    size_t i, j;

    sublen = strlen(substr);
    slen = strlen(str);

    if (!slen || !sublen || sublen > slen)
    {
        return -1;
    }

    for (i = 0; i < slen - sublen; ++i)
    {
            for (j = 0; j < sublen; ++j)
            {
                if (str[i + j] != substr[j])
                {
                    break;
                }
            }
            if (j == sublen)
            {
                return i;
            }
    }

    return streq(str, substr, slen, sublen) ? 0 : -1;
}

int main(int argc, char *argv[])
{
    int start = -1;

    if (argc != 3)
    {
        errx(1, "Usage: str1 str2");
    }

    start = strstrn(argv[1], argv[2]);

    if (start == -1)
    {
        printf("Not Found!\n");
    }
    else
    {
        printf(argv[1]);
        printf("\n");
        for(; start > 0; --start) printf(" ");
        printf("^\n");
    }

    return 0;
}
