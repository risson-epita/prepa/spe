#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include "divisor_sum.h"

int main(int argc, char **argv)
{
   if (argc != 2)
      {
         errx(1, "Error");
      }

   unsigned long param = strtoul(argv[1], NULL, 10);

   if (param == 0)
      {
         errx(1, "Error");
      }

   printf("divisor_sum(%lu) = %lu\n", param, divisor_sum(param));
   return 0;
}
