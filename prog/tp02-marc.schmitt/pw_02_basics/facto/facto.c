unsigned long facto(unsigned long n)
{
   if (n == 0)
      {
         return 1;
      }
   else if (n == 1)
      {
         return 1;
      }
   else if (n == 2)
      {
         return 2;
      }
   else
      {
         return n * facto(n - 1);
      }
}
