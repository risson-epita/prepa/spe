#include <err.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include "htab.h"
 
uint32_t hash(char *key)
{
  uint32_t hash = 0;
  for (size_t i = 0; i < strlen(key); ++i)
  {
    hash += key[i];
    hash += hash << 10;
    hash ^= hash >> 6;
  }
  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;
  return hash;
}

struct htab *htab_new()
{
    struct htab *h = malloc(sizeof(struct htab));
    if(h == NULL) errx(1,"Not enough memory!\n");
    h->capacity = 4;
    h->size = 0;
    h->data = calloc(h->capacity, sizeof(struct pair));
    if(h->data == NULL) errx(1,"Not enough memory!\n");
    h->data[0].next = NULL;
    return h;
}
 
void htab_clear(struct htab *ht)
{
  struct pair *ptr;
   struct pair *tmp;
   for(size_t i = 0; i < ht->capacity; ++i)
   {
       tmp = &(ht->data[i]);
       ptr = tmp;
       while(ptr)
       {
           tmp = tmp->next;
           free(ptr->key);
           free(ptr->value);
           free(ptr->next);
           ptr = tmp;
       }
   }
}
 
void htab_free(struct htab *ht)
{
   htab_clear(ht);
   free(ht->data);
   free(ht);
}
 
struct pair *htab_get(struct htab *ht, char *key)
{
  struct pair *ptr = NULL;
   for(size_t i = 0; i < ht->capacity; ++i)
   {
       ptr = &(ht->data[i]);
       while(ptr != NULL)
       {
           if(strcmp(ptr->key,key)) return ptr;
           ptr = ptr->next;
       }
   }
   return NULL;
}
 
void double_capacity(struct htab* h)
{
    h->data = realloc(h->data, 2 * h->capacity);
    if (h->data == NULL) errx(1, "Not enough memory!");
    for(size_t i = h->capacity; i < h->capacity * 2; i++)
    {
        h->data[i].hkey = 0;
        h->data[i].key = 0;
        h->data[i].value = 0;
        h->data[i].next = NULL;
    }
    h->capacity *= 2;
}
 
int htab_insert(struct htab *ht, char *key, void *value)
{
   if(htab_get(ht, key) != NULL) return 0;
   uint32_t hashv = hash(key);
   struct pair *ptr = &(ht->data[hashv % ht->capacity]);
   struct pair* new = malloc(sizeof(struct pair));
   if (new == NULL) errx(1, "Not enough memory!");
   new->key = key;
   new->value = value;
   new->next = NULL;
   if(ptr->next == NULL)
   {
       ht->size++;
       if ((ht->size / ht-> capacity) * 100 > 75) double_capacity(ht);
       ptr->next = new;
   }
   else
   {
       while(ptr->next != NULL) ptr = ptr->next;
       ptr->next = new;
   }
   return 1;
}
 
void htab_remove(struct htab *ht, char *key)
{
   struct pair *ptr = NULL;
   struct pair *tmp = NULL;
   for(size_t i = 0; i < ht->capacity; i++)
   {
       ptr = &(ht->data[i]);
       while(ptr->next)
       {
           if(strcmp(ptr->next->key, key))
           {
               tmp = ptr->next;
               ptr->next = tmp->next;
               free(tmp);
               return;
           }
       }
   }
}