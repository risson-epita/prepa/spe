#include <stdio.h>
#include <err.h>
#include "mix.h"

int main(int argc, char *argv[])
{
   if (argc != 2)
      {
         char default_text[] = "Votai Test.";
         printf("%s\n", default_text);
         mix(default_text);
         printf("%s\n", default_text);
      }
   else
      {
         printf("%s\n", argv[1]);
         mix(argv[1]);
         printf("%s\n", argv[1]);
      }

   return 0;
}
