unsigned long power_of_two(unsigned long n)
{
   if (n == 0)
      {
         return 1;
      }
   else if (n == 1)
      {
         return 2;
      }
   else
      {
         unsigned long two = 2;
         return two << (n - 1);
      }
}
