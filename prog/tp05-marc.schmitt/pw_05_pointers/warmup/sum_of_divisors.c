# include <stdio.h>

unsigned long isqrt(unsigned long n)
{
   unsigned long r = n;

   while (r * r > n)
      {
         r = r + n / r;
         r = r / 2;
      }

   return r;
}


unsigned long sum_of_divisors(unsigned long n, size_t *count)
{
   if (n <= 6)
      {
         switch (n)
            {
               case 1:
                  *count = 1;
                  return 1;

               case 2:
               case 3:
                  *count = 1;
                  return 1;

               case 4:
                  *count = 2;
                  return 3;

               case 5:
                  *count = 1;
                  return 1;

               case 6:
                  *count = 3;
                  return 6;
            }
      }

   unsigned long result = 0;
   unsigned long sqrt = isqrt(n) + 1;
   unsigned long corresponding_divisor;
   *count = 1;

   for (unsigned long i = 2; i <= sqrt; i++)
      {
         if (n % i == 0)
            {
               result += i;
               ++*count;
               corresponding_divisor = n / i;

               if (corresponding_divisor != i)
                  {
                     result += corresponding_divisor;
                     ++*count;
                  }
            }
      }

   return result + 1;
}

int main()
{
   unsigned long x;
   unsigned long sum;
   size_t count;
   x = 28;
   sum = sum_of_divisors(x, &count);
   printf("x = %lu\n", x);
   printf("sum   = %lu\n", sum);
   printf("count = %zu\n\n", count);
   x = 100;
   sum = sum_of_divisors(x, &count);
   printf("x = %lu\n", x);
   printf("sum   = %lu\n", sum);
   printf("count = %zu\n", count);
}
