#include <stdio.h>
#include "is_perfect_number.h"

int main()
{
   for (unsigned int i = 0; i <= 100000; i++)
      {
         int current = is_perfect_number(i);

         if (current)
            {
               printf("%u\n", i);
            }
      }

   return 0;
}
