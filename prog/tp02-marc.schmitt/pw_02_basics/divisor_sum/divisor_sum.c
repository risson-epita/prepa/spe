#include <stdio.h>

unsigned long isqrt(unsigned long n)
{
   unsigned long r = n;

   while (r * r > n)
      {
         r = r + n / r;
         r = r / 2;
      }

   return r;
}

unsigned long divisor_sum(unsigned long n)
{
   if (n <= 3)
      {
         return 1;
      }

   if (n <= 6)
      {
         switch (n)
            {
               case 4:
                  return 3;

               case 5:
                  return 1;

               case 6:
                  return 6;
            }
      }

   unsigned long result = 0;
   unsigned long sqrt = isqrt(n) + 1;
   unsigned long corresponding_divisor;

   for (unsigned long i = 2; i <= sqrt; i++)
      {
         if (n % i == 0)
            {
               result += i;
               corresponding_divisor = n / i;

               if (corresponding_divisor != i)
                  {
                     result += corresponding_divisor;
                  }
            }
      }

   return result + 1;
}
