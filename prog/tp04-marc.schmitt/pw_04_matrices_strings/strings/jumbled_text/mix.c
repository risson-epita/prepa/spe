#include <stdio.h>

int is_separator(char s)
{
   char separator[] = " ,;:!?./§%*$=+)@_-('&1234567890\"\r\n";
   size_t i = 0;

   while (separator[i] != '\0')
      {
         if (separator[i] == s)
            {
               return 1;
            }

         ++i;
      }

   return 0;
}

size_t strlen(char s[])
{
   size_t i = 0;

   while (s[i] != '\0')
      {
         ++i;
      }

   return i;
}

void mix(char s[])
{
   size_t len = strlen(s);
   size_t j, k;
   char tmp;

   for (size_t i = 0; i < len; ++i)
      {
         if (!is_separator(s[i]))
            {
               j = i;

               while (i < len && !is_separator(s[i]))
                  {
                     ++i;
                  }

               for (k = j + 1; k < i - 2; k += 2)
                  {
                     tmp = s[k];
                     s[k] = s[k + 1];
                     s[k + 1] = tmp;
                  }
            }
      }
}
