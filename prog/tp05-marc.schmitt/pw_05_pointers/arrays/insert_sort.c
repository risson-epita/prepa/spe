#include <stdio.h>
#include "helper.h"

// Insertion using the plain algorithm.
void array_insert(int *begin, int *end, int x)
{
   size_t i = end - begin;
   ++end;

   while (i > 0 && x < * (begin + i - 1))
      {
         *(begin + i) = *(begin + i - 1);
         --i;
      }

   *(begin + i) = x;
}

/*
 * array_bin_search(begin, end, x): search in a sorted array using binary search
 * returns the position of x, or the expected position of x if not present
 */
size_t array_bin_search(int *begin, int *end, int x)
{
   int first = 0;
   int last = end - begin;
   int middle = (first + last) / 2;

   while (first <= last)
      {
         if (*(begin + middle) < x)
            {
               first = middle + 1;
            }
         else if (*(begin + middle) == x)
            {
               return middle;
            }
         else
            {
               last = middle - 1;
            }

         middle = (first + last) / 2;
      }

   if (first > last)
      {
         return middle + 1;
      }

   return middle;
}

// Insertion using the binary-search algorithm.
void array_insert_bin(int *begin, int *end, int x)
{
   size_t i = array_bin_search(begin, end, x);
   size_t j = end - begin;
   ++end;

   while (j > i)
      {
         *(begin + j) = *(begin + j - 1);
         --j;
      }

   *(begin + i) = x;
}

// Insertion sort using plain method.
void array_insert_sort(int *begin, int *end)
{
   size_t n = end - begin;

   for (size_t i = 0; i < n; ++i)
      {
         array_insert(begin, begin + i, *(begin + i));
      }
}

// Insertion sort using binary search.
void array_insert_sort_bin(int *begin, int *end)
{
   size_t n = end - begin;

   for (size_t i = 0; i < n; ++i)
      {
         array_insert_bin(begin, begin + i, *(begin + i));
      }
}
