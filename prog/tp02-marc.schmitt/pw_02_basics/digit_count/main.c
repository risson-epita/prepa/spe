#include <stdio.h>
#include "digit_count.h"

int main()
{
   printf("digit_count(0) = %u\n", digit_count(0));

   for (unsigned long i = 1; i < 9223372036854775808UL; i = i * 2)
      {
         printf("digit_count(%lu) = %u\n", i, digit_count(i));
      }

   printf("digit_count(9223372036854775808) = %u\n",
          digit_count(9223372036854775808UL));
   return 0;
}
