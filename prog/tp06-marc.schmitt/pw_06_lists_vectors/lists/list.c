#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void list_init(struct list *list)
{
   list->next = NULL;
   list->data = 0;
}

int list_is_empty(struct list *list)
{
   return list->next == NULL;
}

size_t list_len(struct list *list)
{
   size_t len = 0;

   while (list->next != NULL)
      {
         list = list->next;
         ++len;
      }

   return len;
}

void list_push_front(struct list *list, struct list *elm)
{
   if (list_is_empty(list))
      {
         list->next = elm;
      }
   else
      {
         struct list *listc = list->next;
         list->next = elm;
         elm->next = k;
      }
}

struct list *list_pop_front(struct list *list)
{
   if (list_is_empty(list))
      {
         return NULL;
      }

   if (list_is_empty(list->next))
      {
         list->next = NULL;
         return list;
      }

   struct list *elm;

   elm = list->next;

   list->next = elm->next;

   return elm;
}

struct list *list_find(struct list *list, int data)
{
   struct list *listc;
   listc = list;

   while (listc != NULL)
      {
         if (listc->data == data)
            {
               return listc;
            }

         listc = listc->next;
      }

   return NULL;
}

struct list *list_lower_bound(struct list *list, int data)
{
   struct list *listc;
   listc = list;

   while (listc->next != NULL)
      {
         if (listc->next->data > data)
            {
               return listc;
            }

         listc = listc->next;
      }

   return NULL;
}

int list_is_sorted(struct list *list)
{
   struct list *listc;
   listc = list;

   while (listc->next != NULL)
      {
         if (listc->data > listc->next->data)
            {
               return 0;
            }
      }

   return 1;
}

void list_insert(struct list *list, struct list *elm)
{
   struct list *listc;
   listc = list;

   while (listc->next != NULL)
      {
         if (listc->next->data > elm->data)
            {
               elm->next = listc->next;
               listc->next = elm;
               break;
            }
      }
}

void list_rev(struct list *list)
{
}

void list_half_split(struct list *list, struct list *second)
{
}
