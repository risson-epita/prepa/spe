__license__ = 'Nathalie (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: prefixTrees.py 2018-02-14'

"""
Prefix Trees homework
2018
@author: marc.schmitt
"""


from algopy import tree


################################################################################
## MEASURES
                        
def countwords(T):
    """ count words in dictionnary T
    
    :param T: The prefix tree
    :rtype: int
    :Example:
    >>> countwords(Tree1)
    11
    """

    count = T.key[1]
    for Tch in T.children:
        count += countwords(Tch)
    return count

def longestwordlength(T):
    """ longest word length
    
    :param T: The prefix tree
    :rtype: int
    :Example:
    >>> longestwordlength(Tree1)
    6
    """
    
    return tree.height(T)

def averagelength(T):
    """ average word length
    
    :param T: The prefix tree
    :rtype: float
    :Example:
    >>> averagelength(Tree1)
    4.636363636363637
    """
    
    return tree.size(T) / countwords(T)

###############################################################################
## Researches

def __compare_letters(letter, key):
    if letter.lower() < key[0].lower():
        return -1
    elif letter.lower() > key[0].lower():
        return 1
    else:
        return 0

def __search_in_children(L, letter):
    """
    return the place of letter in L, or the place where it should be
    """
    (l, r) = (0, len(L))
    while l < r:
        m = l + (r - l) // 2
        c = __compare_letters(letter, L[m].key)
        if c == 0:
            return m
        elif c == -1:
            r = m
        else:
            l = m + 1
    return r

def __search_word(T, word):
    """
    return (i, T, j)
    i: position in word,
    if i == len(word) : T[j].key is the end of the word
    otherwise : T[j] is the place where is / or should be word[i]
    """
    (i, n) = (0, len(word))
    end = False
    while (i < n) and not end:
        j = __search_in_children(T.children, word[i])
        if j != T.nbchildren and \
                        __compare_letters(word[i], T.children[j].key) == 0:
            T = T.children[j]
            i += 1
        else:
            end = True
    return (i, T, j)

def searchword(T, word):
    """ search for a word in dictionary
    
    :param T: The prefix tree
    :param word: The word to search for
    :rtype: bool
    :examples:
    >>> searchword(Tree1, "fabulous")
    False
    >>> searchword(Tree1, "Famous")
    True
    """
    
    (i, T, _) = __search_word(T, word)
    return i == len(word) and T.key[1]

###############################################################################
## Lists

def wordlist(T, pref="", L=[]):
    """ generate the word list
    
    :param T: The prefix tree
    :rtype: list
    :example:
    >>> print(wordlist(Tree1))
    ['case', 'cast', 'castle', 'circle', 'city', 'come', 'could', 'fame', 
                                                    'famous', 'fan', 'fancy']
    """
    word = ""
    word += T.key[0]
    if T.key[1]:
        L.append(word)
    for child in T.children:
        wordlist(child, word, L)
    return L

def longestwords(T):
    """ search for the longest words in dictionary
    
    :param T: The prefix tree
    :rtype: list
    :examples: # order in result does not matter
    >>> longestwords(Tree1)
    ['castle', 'circle', 'famous']
    """
    
    l = []
    L = wordlist(T)
    n = longestwordlength(T)
    for w in L:
        if len(w) == n:
            l.append(w)
    return l
    
def completion(T, prefix):
    """ generate the list of words with a common prefix
    
    :param T: The prefix tree
    :param pref: the prefix
    :rtype: list
    :examples: # result elements can have different case...
    >>> completion(Tree1, "Fan")
    ['Fan', 'Fancy']
    >>> completion(Tree1, "CI")
    ['Circle', 'City']
    >>> completion(Tree1, "what")
    []
    """
    
    (i, T, _) = __search_word(T, prefix)
    if i == len(prefix):
        return wordlist(T, prefix[:-1])
    else:
        return []

def treetofile(T, filename):
    """ save the dictionary in a file
    
    :param T: The prefix tree
    :param filename: the file name
    :example:
    >>> treeToFile(Tree1, "test.txt")
    # give the same file as "textFiles/wordList1.txt" but in alphabetic order
    """
    
    f = open(filename, 'w')
    for word in wordlist(T):
        f.write(word + '\n')
    f.close()

###############################################################################
## Build Tree

def add_child(T, pos, child):
    T.children.append(None)
    for i in range(T.nbchildren-1, pos, -1):
        T.children[i] = T.children[i-1]
    T.children[pos] = child

def addword(T, word):
    """ add a word in dictionary
    
    :param T: The prefix tree
    :param word: The word to add
    """
    
    (i, T, j) = __search_word(T, word)
    n = len(word)
    if i < n:
        add_child(T, j, tree.Tree((word[i], False)))
        T = T.children[j]
        i += 1
        while i < n:
             T.children.append(tree.Tree((word[i], False)))
             T = T.children[-1]
             i += 1
    T.key = (T.key[0], True)

def treefromfile(filename):
    """ build the prefix tree from a file of words
    
    :param filename: The file name
    :rtype: Tree
    """
    
    T = tree.Tree(('', False))        
    f = open(filename)
    f.readline()
    for line in f:
        word = line.strip()
        addword(T, word)
    f.close()
    return T
